------< Arguments >--------------------

Trace file name      :trace1.txt

Page size            :256

Replacement algorithm:fifo

---------------------------------------

---< Trace Information>----------------

Total processes      : 4

Total disk reads     :9529

Total disk writes    :4755

---------------------------------------

-------< Results>----------------------

Total page faults    :9529

Total disk references:14284

Page fault rate      :0.19057999551296234

---------------------------------------
