
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Normalizer.Form;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.DocFlavor.STRING;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.CloseAction;
import sun.awt.SunHints.Value;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * main.java
 *
 * Created on 21 มี.ค. 2557, 13:17:15
 */
/**
 *
 * @author FOX
 *
 */



public class main extends javax.swing.JFrame {
    
    String file;
    int size;
    String algo;
    
    String p1[] = new String[100000];
    String p2[] = new String[100000];
    String p3[] = new String[100000];
    String p4[] = new String[100000];
    
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    
    
    public String ram[] = new String[8];
    public int dirty[] = new int[8];
    public int clru[] = new int[8];
    
    public String ram512[] = new String[7];
    public int dirty512[] = new int[7];
    public int clru512[] = new int[7];
    
    public String ram1024[] = new String[6];
    public int dirty1024[] = new int[6];
    public int clru1024[] = new int[6];
    
    
    
    int countlru=0;
    
    int countfall=0;
    
    int countwrite=0;
    
    float countreadfile=0;
    
    boolean tf = false;
    
    
    
    public int searchmin(){
       int min=0;
       
       if(size==256){
           for (int i = 0; i < ram.length; i++) {
           if(clru[min]>clru[i]){
               min = i;
           }
           
           
       }
       }
       else if(size==512){
           for (int i = 0; i < ram512.length; i++) {
           if(clru512[min]>clru512[i]){
               min = i;
           }
           
           
       }
       }
       else if(size==1024){
           for (int i = 0; i < ram1024.length; i++) {
           if(clru1024[min]>clru1024[i]){
               min = i;
           }
           
           
       }
       }
       
       
       return min;
   }
    
    public void doswap(){
        String swapS;
        
        if(size==256){
            swapS=ram[0];
        
        for (int i = 0; i <ram.length-1; i++) {
            ram[i]=ram[i+1];
            dirty[i]=dirty[i+1];
        }
        ram[ram.length-1]=swapS;
        dirty[dirty.length-1]=0;
        }
        else if (size==512){
            swapS=ram512[0];
        
        for (int i = 0; i <ram512.length-1; i++) {
            ram512[i]=ram512[i+1];
            dirty512[i]=dirty512[i+1];
        }
        ram512[ram512.length-1]=swapS;
        dirty512[dirty512.length-1]=0;
        }
        else if (size==1024){
            swapS=ram1024[0];
        
        for (int i = 0; i <ram1024.length-1; i++) {
            ram1024[i]=ram1024[i+1];
            dirty1024[i]=dirty1024[i+1];
        }
        ram1024[ram1024.length-1]=swapS;
        dirty1024[dirty1024.length-1]=0;
        }
        
        
        
        
        
    }
    
    public void fifo(String bit,String dir){
        
        if(size==256){
            if(dirty[0]==1){
                countwrite++;
            }
            for (int i = 0; i <ram.length-1; i++) {
            ram[i]=ram[i+1];
            dirty[i]=dirty[i+1];
        }
        ram[ram.length-1]=bit;
        if("W".equals(dir)){
                    dirty[dirty.length-1]=1;
                }
                    
                else{
                    dirty[dirty.length-1]=0;
                }
        
        }
        else if(size==512){
            if(dirty512[0]==1){
                countwrite++;
            }
            for (int i = 0; i <ram512.length-1; i++) {
            ram512[i]=ram512[i+1];
            dirty512[i]=dirty512[i+1];
        }
        ram512[ram512.length-1]=bit;
        if("W".equals(dir)){
                    dirty512[dirty512.length-1]=1;
                }
                    
                else{
                    dirty512[dirty512.length-1]=0;
                }
        
        }
        else if(size==1024){
            if(dirty1024[0]==1){
                countwrite++;
            }
            for (int i = 0; i <ram1024.length-1; i++) {
            ram1024[i]=ram1024[i+1];
            dirty1024[i]=dirty1024[i+1];
        }
        ram1024[ram1024.length-1]=bit;
        if("W".equals(dir)){
                    dirty1024[dirty1024.length-1]=1;
                }
                    
                else{
                    dirty1024[dirty1024.length-1]=0;
                }
        
        }
        
        
       countfall++;
    }
    
    public void sec(String bit,String dir){
        if(size==256){
            if(dirty[0]==1){
            doswap();
            sec(bit, dir);
            countwrite++;
        }
        else{
            fifo(bit, dir);
        }
        }
        else if(size==512){
            if(dirty512[0]==1){
            doswap();
            sec(bit, dir);
            countwrite++;
        }
        else{
            fifo(bit, dir);
        }
        }
        else if(size==1024){
            if(dirty1024[0]==1){
            doswap();
            sec(bit, dir);
            countwrite++;
        }
        else{
            fifo(bit, dir);
        }
        }
        
        
    }
    
    public void lru(String bit,String dir){
        int min = searchmin();
        if(size==256){
            if(dirty[min]==1){
                countwrite++;
            }
            ram[min]=bit;
            if("W".equals(dir)){
                    dirty[min]=1;
                }
                    
                else{
                    dirty[min]=0;
                }
        }
        else if(size==512){
            if(dirty512[min]==1){
                countwrite++;
            }
            ram512[min]=bit;
            if("W".equals(dir)){
                    dirty512[min]=1;
                }
                    
                else{
                    dirty512[min]=0;
                }
        }
        else if(size==1024){
            if(dirty1024[min]==1){
                countwrite++;
            }
            ram1024[min]=bit;
            if("W".equals(dir)){
                    dirty1024[min]=1;
                }
                    
                else{
                    dirty1024[min]=0;
                }
        }    
        
        countfall++;
    }
    
   
    

    
    
    public void replacement(String bit,String dir){
        if("fifo".equals(algo)){
            fifo(bit, dir);    
        }
        else if("sec".equals(algo)){
            sec(bit, dir);
        }
        else if("lru".equals(algo)){
            lru(bit,dir);
        }
        
    }
    
    
    
    public void searchram(String bit){
        
        
        if(size==256){
            for (int k = 0; k < ram.length; k++) {
            
            if(ram[k] == null ? bit == null : ram[k].equals(bit)){
                tf=true;
                clru[k]=countlru++;
            }
            
         }
        }
        else if (size==512){
            for (int k = 0; k < ram512.length; k++) {
            
            if(ram512[k] == null ? bit == null : ram512[k].equals(bit)){
                tf=true;
                clru512[k]=countlru++;
            }
            
         }
        }
        else if (size==1024){
            for (int k = 0; k < ram1024.length; k++) {
            
            if(ram1024[k] == null ? bit == null : ram1024[k].equals(bit)){
                tf=true;
                clru1024[k]=countlru++;
            }
            
         }
        }
        
            
        
        
        
        
    }
    
    
    public void placeram(String bit,String dir){
        
        
        if(size==256){
            for (int k = 0; k < ram.length; k++) {

            if(ram[k] == null){
                ram[k]=bit;
                if("W".equals(dir)){
                    dirty[k]=1;
                }
                    
                else{
                    dirty[k]=0;
                }
                
                clru[k]=countlru++;
                    
                break;
            }
            
         }
        }
        else if(size==512){
        for (int k = 0; k < ram512.length; k++) {

            if(ram512[k] == null){
                ram512[k]=bit;
                if("W".equals(dir)){
                    dirty512[k]=1;
                }
                    
                else{
                    dirty512[k]=0;
                }
                
                clru512[k]=countlru++;
                    
                break;
            }
            
         }
        }
        else if(size==1024){
        for (int k = 0; k < ram1024.length; k++) {

            if(ram1024[k] == null){
                ram1024[k]=bit;
                if("W".equals(dir)){
                    dirty1024[k]=1;
                }
                    
                else{
                    dirty1024[k]=0;
                }
                
                clru1024[k]=countlru++;
                    
                break;
            }
            
         }
    }
            
        
        
        
        
        
    }
    
    
    
    public void gettext(String file){
        
        
        
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            System.out.println("Flie nor found");
            System.exit(0);
        }
            String line;
            
        try {
            while ((line = br.readLine()) != null) {
                
                if("".equals(line)){
                    break;
                }
                
                String spilt[]=line.split(" ");
                
                if("1".equals(spilt[0])){
                    p1[i1++]=line;
                }
                else if("2".equals(spilt[0])){
                    p2[i2++]=line;
                }
                else if("3".equals(spilt[0])){
                    p3[i3++]=line;
                }
                else if("4".equals(spilt[0])){
                    p4[i4++]=line;
                }
                
                //System.out.println(Integer.toBinaryString(Integer.valueOf(spilt[1])));
                
                String bit = Integer.toBinaryString(Integer.valueOf(spilt[1]));
                
                
                if(bit.length()!=8){
                    int len=bit.length();
                    for (int i = 0; i < 8-len; i++) {
                        bit="0"+bit;
                        
                    }
                        
                    
                }
                
                
                
                
                searchram(bit.substring(0,8));
                    
                    if(tf==false){
                        
                        //System.out.println(tf);
                        
                        searchram(null);
                       
                        if(tf==true){
                            placeram(bit.substring(0,8),spilt[3]);
                        }
                        else{
                            replacement(bit.substring(0,8),spilt[3]);
                        }
                    }
                    
                tf=false;
                    
                
                countreadfile++;
            }
            
            
            /*for (int k = 0; k < ram.length; k++) {
                    System.out.println(ram[k]);
                
                }*/
        } catch (IOException ex) {
            System.out.println("Flie nor found");
        }
        try {
            br.close();
        } catch (IOException ex) {
            System.out.println("Flie nor found");
        }
           
    }

    /** Creates new form main */
    public main() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        filebox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        algobox = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        sizebox = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Size");

        filebox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "trace1.txt", "trace2.txt" }));
        filebox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileboxActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Algotiyhm");

        algobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "fifo", "sec", "lru" }));

        jButton1.setText("OK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("File");

        sizebox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "256", "512", "1024" }));
        sizebox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sizeboxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(algobox, 0, 140, Short.MAX_VALUE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(sizebox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(filebox, 0, 140, Short.MAX_VALUE)))
                .addContainerGap(190, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(filebox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sizebox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(algobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(122, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fileboxActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        file=filebox.getSelectedItem().toString();

        size=Integer.valueOf(sizebox.getSelectedItem().toString());
        
        
        algo=algobox.getSelectedItem().toString();
        
        
        gettext(file);
        
        System.out.println(countreadfile);
        
        int diskref=countfall+countwrite;
        
        double pfr=countfall/countreadfile;
        
        System.out.println(pfr);
        
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("output"+file+".txt"));
            
            out.write("------< Arguments >--------------------"+"\n");
            out.newLine();
            out.write("Trace file name      :"+file+"\n");
            out.newLine();
            out.write("Page size            :"+size+"\n");
            out.newLine();
            out.write("Replacement algorithm:"+algo+"\n");
            out.newLine();
            out.write("---------------------------------------\n");
            out.newLine();
            out.write("---< Trace Information>----------------\n");
            out.newLine();
            out.write("Total processes      : 4\n");
            out.newLine();
            out.write("Total disk reads     :"+countfall+"\n");
            out.newLine();
            out.write("Total disk writes    :"+countwrite+"\n");
            out.newLine();
            out.write("---------------------------------------\n");
            out.newLine();
            out.write("-------< Results>----------------------\n");
            out.newLine();
            out.write("Total page faults    :"+countfall+"\n");
            out.newLine();
            out.write("Total disk references:"+diskref+"\n");
            out.newLine();
            out.write("Page fault rate      :"+pfr+"\n");
            out.newLine();
            out.write("---------------------------------------\n");
             
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        System.out.println(countfall);
        
        System.exit(0);
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void sizeboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sizeboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sizeboxActionPerformed

    /*
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            
            

            public void run() {
                new main().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox algobox;
    private javax.swing.JComboBox filebox;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JComboBox sizebox;
    // End of variables declaration//GEN-END:variables
}
